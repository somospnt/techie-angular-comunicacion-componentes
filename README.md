# Comunicación entre componentes de Angular #

Este proyecto es una pequeña actividad para aprender a comunicar compomentes a través de un componente padre en Angular 2+.

* Actividad a realizar en rama [master](https://bitbucket.org/somospnt/techie-angular-comunicacion-componentes/src/master/).

* Actividad completada en rama  [techie-finalizada](https://bitbucket.org/somospnt/techie-angular-comunicacion-componentes/src/techie-finalizada/).

---

### Dependencias ###

Para correr este proyecto es necesario instalar las siguientes dependencias:

* [Node.js](https://nodejs.org/es/download/)

* [Angular CLI](https://cli.angular.io/)

```
npm install -g @angular/cli
```

---


### Uso ###

En la carpeta raiz del proyecto correr:
```
npm install
ng serve --open
```

---

### Recursos ###

* Un artículo cortito sobre este tema se puede encontrar [aquí](https://medium.com/dailyjs/3-ways-to-communicate-between-angular-components-a1e3f3304ecb).

* [Repo con un proyecto demo que también usa servicios para comunicarse](https://bitbucket.org/somospnt/angular-comm-entre-componentes/src/master/).