import { Component } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent {
  texto: string;
  textos: string[] = ['Me gusta el arrrrrrrte', 'A veces armo cossssssas', 'No porque mi familia es judío'];

  cambiarTexto() {
    const numeroRandom = Math.floor((Math.random() * 3));
    this.texto = this.textos[numeroRandom];
  }
}
