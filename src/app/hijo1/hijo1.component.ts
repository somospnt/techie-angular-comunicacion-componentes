import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-hijo1',
  templateUrl: './hijo1.component.html',
  styleUrls: ['./hijo1.component.css']
})
export class Hijo1Component {
  @Output() evento = new EventEmitter<any>();
  cambiarTexto() {
    this.evento.emit();
  }
}
